int light = false;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  light = analogRead(A0);
  Serial.println(light, DEC);  
  (0 == light)?encenderLed():apagarLed();
  
  delay(1000);
}

void encenderLed(){
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
}

void apagarLed(){
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW  
}
